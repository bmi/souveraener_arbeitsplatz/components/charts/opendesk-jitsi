{{/*
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
*/}}
{{/* vim: set filetype=mustache: */}}
{{- define "opendesk-jitsi.keycloakAdapter.fullname" -}}
{{ include "jitsi-meet.name" . }}-keycloak-adapter
{{- end -}}
